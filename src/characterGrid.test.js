const mockFs = require('mock-fs');
const {
  findWordsInCharGrid,
  findWordsInColumns,
  findWordsInRows,
  findWordsOnDiagonalLTR,
  findWordsOnDiagonalRTL,
  formatOutput,
  processInput,
} = require('./characterGrid');

describe('process input file', () => {
  it('process out dimensions, character grid, and words to search for', async () => {
    // set up mock file to read in
    mockFs({
      src: {
        'example.txt': '3x3\nA B C\nD E F\nG H I\nABC\nAEI',
      },
    });

    const { dimensions, charGrid, wordsToSearchFor } = await processInput('src/example.txt');

    expect(dimensions.numRows).toEqual(3);
    expect(dimensions.numCols).toEqual(3);
    expect(charGrid).toEqual(['ABC', 'DEF', 'GHI']);
    expect(wordsToSearchFor).toEqual(['ABC', 'AEI']);

    mockFs.restore();
  });
});

describe('search for words', () => {
  describe('horizontal search', () => {
    it('find words in rows', () => {
      const charGrid = ['ABC', 'DEF', 'GHI'];
      const wordsToSearchFor = ['ABC', 'AEI'];

      const rowResults = findWordsInRows(charGrid, wordsToSearchFor);

      expect(rowResults).toEqual([{
        word: 'ABC', startRow: 0, endRow: 0, startCol: 0, endCol: 2,
      }]);
    });

    it('find words in rows spelled backwards', () => {
      const charGrid = ['CBA', 'DEF', 'GHI'];
      const wordsToSearchFor = ['ABC', 'AEI'];

      const rowResults = findWordsInRows(charGrid, wordsToSearchFor);

      expect(rowResults).toEqual([{
        word: 'ABC', startRow: 0, endRow: 0, startCol: 2, endCol: 0,
      }]);
    });
  });

  describe('vertical search', () => {
    it('find words in columns', () => {
      const charGrid = ['ABC', 'DEF', 'GHI'];
      const wordsToSearchFor = ['BEH', 'AEI'];

      const colResults = findWordsInColumns(charGrid, wordsToSearchFor);

      expect(colResults).toEqual([{
        word: 'BEH', startRow: 0, endRow: 2, startCol: 1, endCol: 1,
      }]);
    });

    it('find words in columns spelled backwards', () => {
      const charGrid = ['AHC', 'DEF', 'GBI'];
      const wordsToSearchFor = ['BEH', 'AEI'];

      const colResults = findWordsInColumns(charGrid, wordsToSearchFor);

      expect(colResults).toEqual([{
        word: 'BEH', startRow: 2, endRow: 0, startCol: 1, endCol: 1,
      }]);
    });
  });

  describe('diagonal search', () => {
    describe('left to right diagonal search', () => {
      it('finds words diagonally left to right', () => {
        const dimensions = { numRows: 3, numCols: 3 };
        const charGrid = ['ABC', 'DEF', 'GHI'];
        const wordsToSearchFor = ['BEH', 'AEI'];

        const colResults = findWordsOnDiagonalLTR(dimensions, charGrid, wordsToSearchFor);

        expect(colResults).toEqual([{
          word: 'AEI', startRow: 0, endRow: 2, startCol: 0, endCol: 2,
        }]);
      });

      it('finds words diagonally left to right spelled backwards', () => {
        const dimensions = { numRows: 3, numCols: 3 };
        const charGrid = ['IBC', 'DEF', 'GHA'];
        const wordsToSearchFor = ['BEH', 'AEI'];

        const colResults = findWordsOnDiagonalLTR(dimensions, charGrid, wordsToSearchFor);

        expect(colResults).toEqual([{
          word: 'AEI', startRow: 2, endRow: 0, startCol: 2, endCol: 0,
        }]);
      });
    });

    describe('right to left diagonal search', () => {
      it('finds words diagonally right to left', () => {
        const dimensions = { numRows: 3, numCols: 3 };
        const charGrid = ['ABC', 'DEF', 'GHI'];
        const wordsToSearchFor = ['BEH', 'CEG'];

        const colResults = findWordsOnDiagonalRTL(dimensions, charGrid, wordsToSearchFor);

        expect(colResults).toEqual([{
          word: 'CEG', startRow: 0, endRow: 2, startCol: 2, endCol: 0,
        }]);
      });

      it('finds words diagonally right to left spelled backwards', () => {
        const dimensions = { numRows: 3, numCols: 3 };
        const charGrid = ['ABG', 'DEF', 'CHI'];
        const wordsToSearchFor = ['BEH', 'CEG'];

        const colResults = findWordsOnDiagonalRTL(dimensions, charGrid, wordsToSearchFor);

        expect(colResults).toEqual([{
          word: 'CEG', startRow: 2, endRow: 0, startCol: 0, endCol: 2,
        }]);
      });
    });
  });
});

describe('display results', () => {
  it('turns found word into output to display to users', () => {
    const foundWords = [
      {
        word: 'AEI',
        startRow: 2,
        endRow: 0,
        startCol: 2,
        endCol: 0,
      },
      {
        word: 'CEG',
        startRow: 2,
        endRow: 0,
        startCol: 0,
        endCol: 2,
      },
    ];
    const expectedDisplayOutput = ['AEI 2:2 0:0', 'CEG 2:0 0:2'];

    const displayOutput = formatOutput(foundWords);

    expect(displayOutput).toEqual(expectedDisplayOutput);
  });

  it('find words in character grid', async () => {
    // set up mock file to read in
    mockFs({
      src: {
        'example.txt': '3x3\nA B C\nD E F\nG H I\nABC\nAEI',
      },
    });
    const expectedFoundWords = ['ABC 0:0 0:2', 'AEI 0:0 2:2'];

    const foundWords = await findWordsInCharGrid('src/example.txt');

    expect(foundWords).toEqual(expectedFoundWords);

    mockFs.restore();
  });
});
