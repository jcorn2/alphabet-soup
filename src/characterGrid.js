const fs = require('fs');
const readLine = require('readline');

/**
 * Processes a file for the character grid and the words to search for in it.
 * @param {string} file the name of the file that contains the input data
 * @return {Object} returns an object that contains the dimensions of the
 * character grid, the character grid as an array containing all the rows as
 * strings, and an array of the words to search for.
 */
const processInput = (file) => {
  const lines = [];

  const streamInterface = readLine.createInterface({
    input: fs.createReadStream(file, { encoding: 'ascii' }),
    output: null,
    console: false,
  });

  // reads in all lines in file
  streamInterface.on('line', (line) => {
    lines.push(line);
  });

  return new Promise((resolve) => {
    streamInterface.on('close', () => {
      // extracts dimensions, character grid, and words to search for from the input file
      const dimensionsAsString = lines[0].split('x');
      const dimensions = {
        numRows: parseInt(dimensionsAsString[0], 10), numCols: parseInt(dimensionsAsString[1], 10),
      };
      const charGrid = lines.slice(1, dimensions.numRows + 1).map((line) => line.replace(/\s+/g, ''));
      const wordsToSearchFor = lines.slice(dimensions.numRows + 1);

      resolve({ dimensions, charGrid, wordsToSearchFor });
    });
  });
};

/**
 * Searches horizontally for words in the character grid
 * @param {Array.<string>} charGrid an array where each element is a row in the
 * character grid
 * @param {Array.<string>} wordsToSearchFor the words to search for in the
 * character grid
 * @return {Array.<string>} the words found in the character grid horizontally
 * with their start and stop indices
 */
const findWordsInRows = (charGrid, wordsToSearchFor) => {
  const foundWords = charGrid.reduce((currentFoundWords, row, rowIndex) => {
    const wordsInRow = wordsToSearchFor.filter((word) => row.includes(word)).map((word) => {
      const startIndexOfWord = row.indexOf(word);
      return {
        word,
        startRow: rowIndex,
        endRow: rowIndex,
        startCol: startIndexOfWord,
        endCol: startIndexOfWord + word.length - 1,
      };
    });
    // search for words backwards in the row
    const reverseWords = wordsToSearchFor.map((word) => ({
      originalWord: word, reversedWord: word.split('').reverse().join(''),
    }));
    const reverseWordsInRow = reverseWords.filter(({ reversedWord }) => row.includes(reversedWord))
      .map(({ originalWord, reversedWord }) => {
        const startIndexOfWord = row.indexOf(reversedWord);
        return {
          word: originalWord,
          startRow: rowIndex,
          endRow: rowIndex,
          startCol: startIndexOfWord + originalWord.length - 1,
          endCol: startIndexOfWord,
        };
      });

    // add found words to array of previously found words
    return [...currentFoundWords, ...wordsInRow, ...reverseWordsInRow];
  }, []);

  return foundWords;
};

/**
 * Searches vertically for words in the character grid by inverting the
 * character grid and reusing findWordsInRows to find words.
 * @param {Array.<string>} charGrid an array where each element is a row in the
 * character grid
 * @param {Array.<string>} wordsToSearchFor the words to search for in the
 * character grid
 * @return {Array.<string>} the words found in the character grid vertically
 * with their start and stop indices
 */
const findWordsInColumns = (charGrid, wordsToSearchFor) => {
  const invertedCharGrid = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < charGrid.length; i += 1) {
    invertedCharGrid[i] = charGrid.map((row) => row[i]).join('');
  }

  const invertedRowResults = findWordsInRows(invertedCharGrid, wordsToSearchFor);

  // adjusts row and col indecies from inverted grid to original grid
  const colResults = invertedRowResults.reduce((acc, cur) => {
    const colResult = {
      ...cur,
      startRow: cur.startCol,
      endRow: cur.endCol,
      startCol: cur.startRow,
      endCol: cur.endRow,
    };

    return [...acc, colResult];
  }, []);

  return colResults;
};

/**
 * Searches diagonally left to right for words in the character grid.
 * @param {Array.<string>} charGrid an array where each element is a row in the
 * character grid
 * @param {Array.<string>} wordsToSearchFor the words to search for in the
 * character grid
 * @return {Array.<string>} the words found in the character grid diagonally
 * left to right with their start and stop indices
 */
const findWordsOnDiagonalLTR = (dimensions, charGrid, wordsToSearchFor) => {
  const { numRows, numCols } = dimensions;
  const invertedCharGrid = [];
  for (let i = 0; i < charGrid.length; i += 1) {
    invertedCharGrid[i] = charGrid.map((row) => row[i]).join('');
  }

  // extract diagonals from character grid
  const gridAsOneLine = invertedCharGrid.join('').split('');
  const gridDiagonals = [];
  // diagonals below diagonal starting at 0:0
  for (let i = 0; i < numRows; i += 1) {
    const lettersOnDiagonal = [];
    for (let j = 0; i + j < numRows; j += 1) {
      lettersOnDiagonal.push(gridAsOneLine[i + (j * (numRows + 1))]);
    }
    gridDiagonals.push({ letters: lettersOnDiagonal.join(''), startRow: i, startCol: 0 });
  }

  // diagonals above diagonal starting at 0:0
  for (let j = 1; j < numCols; j += 1) {
    const lettersOnDiagonal = [];
    for (let i = 0; i + j < numRows; i += 1) {
      lettersOnDiagonal.push(
        gridAsOneLine[(i * (numRows + 1)) + (j * (numRows))],
      );
    }
    gridDiagonals.push({ letters: lettersOnDiagonal.join(''), startRow: 0, startCol: j });
  }

  const foundWords = gridDiagonals.reduce((currentFoundWords, diagonal, rowIndex) => {
    const wordsInRow = wordsToSearchFor.filter((word) => diagonal.letters.includes(word))
      .map((word) => {
        const startIndexOfWord = diagonal.letters.indexOf(word);
        // adjust position back to original grid coordinates
        return {
          word,
          startRow: rowIndex,
          endRow: rowIndex + word.length - 1,
          startCol: startIndexOfWord,
          endCol: startIndexOfWord + word.length - 1,
        };
      });

    // search for words backwards in the row
    const reverseWords = wordsToSearchFor.map((word) => ({
      originalWord: word, reversedWord: word.split('').reverse().join(''),
    }));
    const reverseWordsInRow = reverseWords.filter(({ reversedWord }) => diagonal.letters
      .includes(reversedWord))
      .map(({ originalWord, reversedWord }) => {
        const startIndexOfWord = diagonal.letters.indexOf(reversedWord);
        // adjust position back to original grid coordinates
        return {
          word: originalWord,
          startRow: rowIndex + originalWord.length - 1,
          endRow: rowIndex,
          startCol: startIndexOfWord + originalWord.length - 1,
          endCol: startIndexOfWord,
        };
      });

    // add found words to array of previously found words
    return [...currentFoundWords, ...wordsInRow, ...reverseWordsInRow];
  }, []);
  return foundWords;
};
/**
 * Searches diagonally right to left for words in the character grid.
 * @param {Array.<string>} charGrid an array where each element is a row in the
 * character grid
 * @param {Array.<string>} wordsToSearchFor the words to search for in the
 * character grid
 * @return {Array.<string>} the words found in the character grid diagonally
 * right to left with their start and stop indices
 */
const findWordsOnDiagonalRTL = (dimensions, charGrid, wordsToSearchFor) => {
  // flip character grid so can search for words left to right using findWordsOnDiagonalLTR
  const flippedGrid = charGrid.map((row) => row.split('').reverse().join(''));

  const foundWords = findWordsOnDiagonalLTR(dimensions, flippedGrid, wordsToSearchFor);

  // adjusts row and col indecies from flipped grid to original grid
  const adjustedPositions = foundWords.reduce((acc, cur) => {
    const colResult = {
      ...cur,
      startCol: cur.endCol,
      endCol: cur.startCol,
    };

    return [...acc, colResult];
  }, []);

  return adjustedPositions;
};

/**
 * Format positions of found words for display
 * @param {Array.<Object>} foundWords the words that were found in the grid
 */
const formatOutput = (foundWords) => foundWords.map((foundWord) => `${foundWord.word} ${foundWord.startRow}:${foundWord.startCol} ${foundWord.endRow}:${foundWord.endCol}`);

/**
 * Performs a word search for words in a character grid defined in a file.
 * @param {string} file the file that contains the character grid and the words
 * to search for
 * @return {Promise} resolves to array containing the found words and their
 * positions in the character grid.
 */
const findWordsInCharGrid = async (file) => {
  const { dimensions, charGrid, wordsToSearchFor } = await processInput(file);
  const rowResults = findWordsInRows(charGrid, wordsToSearchFor);
  const colResults = findWordsInColumns(charGrid, wordsToSearchFor);
  const colResultsLTR = findWordsOnDiagonalLTR(dimensions, charGrid, wordsToSearchFor);
  const colResultsRTL = findWordsOnDiagonalRTL(dimensions, charGrid, wordsToSearchFor);
  const allResults = [...rowResults, ...colResults, ...colResultsLTR, ...colResultsRTL];
  allResults.sort((a, b) => wordsToSearchFor.indexOf(a.word) - wordsToSearchFor.indexOf(b.word));

  return formatOutput(allResults);
};

module.exports = {
  findWordsInCharGrid,
  findWordsInColumns,
  findWordsInRows,
  findWordsOnDiagonalLTR,
  findWordsOnDiagonalRTL,
  formatOutput,
  processInput,
};
