const { findWordsInCharGrid } = require('./characterGrid');

const fileName = process.argv[2];

findWordsInCharGrid(fileName).then((foundWords) => {
  // need to disable no-console because requirement is to output the results to the screen
  // eslint-disable-next-line no-console
  foundWords.forEach((word) => console.log(word));
});
